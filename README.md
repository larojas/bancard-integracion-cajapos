# bancard-integracion-cajapos
|__ **Linux**<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **driver-linux-Mint-18-2.zip** - Driver de cable para Mint 18.2<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **x64.zip** - Backend y Simulador para 64 bits<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **x86.zip** - Backend y Simulador para 32 bits<br>
|__ **Windows**<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **DRIVER_USB_HL340.zip** - Driver del cable del POS<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **SrvStart.zip** - Archivos necesarios para agregar como Servicio de S.O<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **x64.zip** - Backend y Simulador para 64 bits<br>
|&nbsp;&nbsp;&nbsp;&nbsp;|__ **x86.zip** - Backend y Simulador para 32 bits<br>
|__ **DocumentacionIntegracionCajaPOS.pdf** - Documentación del SDK<br>
|__ **README.md** - Estructura del directorio<br>
|__ **cajapos1.5.0.postman_collection.json** - Ejemplos de POSTMAN para pruebas